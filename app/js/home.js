    var mediaPath = "./media/";    
    var media  = [];
    var videoExtension = ["mp4"];
    var audioExtension = ["mp3"];
    var totalResults = 0;
    var data = [
                {
                   id :"d02.mp3",
                   delay : 200,
                   isFirst: true,  
                }];

/**
 * [loadPage Carga la pagina seleccionada]
 * @param  {[type]} url [description]
 * @return {[type]}     [description]
 */
function loadPage(url){     
  stopAll();
  jQuery(".container-wrapper").load(url,function(){
    //stopAll();
  });
}
/**
 * [updateAvance Actualiza la barra de carga en modal]
 * @param  {[type]} width [description]
 * @return {[type]}       [description]
 */
function updateAdvance(width){
  
   jQuery("#progress-bar","#modalLoading").css({width:width});
   jQuery("#avance","#modalLoading").html(width);           
  
   return true;
}

/**
 * [stopAll Pausa todos los audios del DOM]
 * @return {[type]} [description]
 */
function stopAll(){
  
  var elmentsAudio = document.getElementsByClassName("audio-item");
  var elmentsVideo = document.getElementsByClassName("video-item");    
  
    
  for (var i = 0; i < elmentsAudio.length; i++) {       
           elmentsAudio[i].pause();
           elmentsAudio[i].currentTime = 0;       
  }
  
  for (var y = 0; y < elmentsVideo.length; y++) {      
          elmentsVideo[y].pause();
          elmentsVideo[y].currentTime = 0;   
   
  }

  return true;
}
/**
 * [mediaContent Crea y carga los elementos de audio y Video en el DOM
 *               uno por uno  "Recursiva" ]
 * @param  {[Array]} media [Con el los audios]
 * @return {[type]}       [description]
 */
function mediaContent(media){

  var thisFn = arguments.callee;
  
  if( media && media.length > 0){
    var file    = media[0].split(".");
  
    var fileName = file[0];
    var fileExtension = file[1];
    var typeElement = "audio";
    var className   = "audio-item";
    var contentType   = "audio/mpeg";
   
    if(videoExtension.indexOf(fileExtension) != -1){
        typeElement = "video";
        className   = "video-item";
        contentType = "video/mpeg";
    }

    
    var element = document.createElement(typeElement);
    if( typeElement == 'video'){
        element.width  = "0";
        element.height = "0";
    }

    element.src = mediaPath+media[0];
    element.id = media[0];
    element.className = className;
    element.type = contentType;
    element.load();
          
    media.shift();

    /*loadeddata*/
    element.addEventListener("loadeddata",function(){

      if(element.readyState) 
       var rest  = (totalResults - media.length);
       var width = Math.round((rest * 100) / totalResults);
       
       updateAdvance(width+"%")
       //call recursive
       thisFn(media);

    },false);
    
    document.getElementById("reproductor").appendChild(element);         
   
  }else{
      //completed
      jQuery("#modalLoading").modal("hide");
      
      Audio.init(data);
  }
}
/**
 * [loadResources Obtiene todos los elementos de la carpeta de audio]
 * @return {[type]} [description]
 */
function loadResources(){
    
    jQuery("#modalLoading").modal("show");   
    var media = ["d02.mp3","d03.mp3","d04-01.mp3","d04-02.mp3","d04-03.mp3","d04-04.mp3","d05.mp3","d06-01.mp3","d06-02.mp3","d06-03.mp3","d06-04.mp3","d06.mp3",
                 "d07-01.mp3","d07-02.mp3","d07-03.mp3","d07-04.mp3","d07-05.mp3","d07-06.mp3","d07.mp3","d08-01.mp3","d08-02.mp3","d08-03.mp3","d08-04.mp3","d08-05.mp3"];
    totalResults = media.length;
    
    mediaContent(media);
}




jQuery(document).ready(function(){          

    jQuery(".container-wrapper").delegate("a:not(.notClicked)","click",function(event){
        event.preventDefault();
        event.stopPropagation();
        var href = jQuery(this).attr("href");          
        loadPage(href);

    });
 
});
jQuery('#modalLoading').on('hidden.bs.modal', function (e) {
  updateAdvance('0%');
});
