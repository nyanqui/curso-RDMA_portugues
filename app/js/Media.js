var Media = {
	data : null,
	container:null,
		
	init : function(options){
		var self = this;
		
		if(!options){
			console.log("can't init");
			return false;
		}
		self.container =  options.container;
		self.data = options.data;
		if(options.autoplay){
			self.run();
		}


	},
	run : function(){
		
		var self = this;
		self.getMediaContent().innerHtml="";
		if(self.data && self.data != ""){
			
			var media = self.data;
			var delay = (media.delay)? data.delay : 0;
		
			if( data.src != "" && media.dataType != "" ){			
		
				self.getVideoElement().firstChild.src  = media.src;
				self.getVideoElement().firstChild.type = media.dataType;	

				setTimeout(function(){
					self.getVideoElement().play();
				},delay);
				
			}
			
		}

	},
	getVideoElement : function(){
		var self = this;
		
		if(!self.contentElement){
			var element     = document.createElement("video");			
			var source  	= document.createElement("source");

				element.width  = "800"; 
				element.height = "400";
				source.type = "video/mp4";
				source.src  = "";

			element.appendChild(source);
			
			self.contentElement = element;
			self.getMediaContent().appendChild(element);
		}

		return self.contentElement;	  

	},
	getMediaContent : function(){
		var self = this;
	
		if(!self.mediaContent){
			
			var container = document.getElementById("containerVideo");				
				container.innerHTML = "";
		
			var element = document.createElement("div");
				element.id = "media-container";
				element.className = "seccion-video";
				container.appendChild(element);

			self.mediaContent = element;			
		}

		return self.mediaContent;
	}
}