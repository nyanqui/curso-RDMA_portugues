$(document).ready(function(){

    var durante8 = function () {
        var rptas = ["1", "4", "8", "11", "15", "18"];
        var contestadas = [null, null, null, null, null, null];
       
        var validate = function (audio) {
            var totalRptas = rptas.length;
            var rptasContestadas = 0;

            $(".durante8 .content-btn").each( function (i) {
                if ($(this).find(".active-btn-green").length) {
                    var rpta = $(this).find(".active-btn-green").data("value");
                    rptasContestadas++;
                    
                    if (rpta == rptas[i]) {
                        contestadas[i] = true;
                    } else {
                        contestadas[i] = false;
                    }

                 
                    if (totalRptas == rptasContestadas) {
                        var correcto = true;

                        $.each( contestadas, function( key, value ) {
                            if (!value) {
                                correcto = false;
                                return false;
                            }
                        });
                       
                        if(audio){            
                            audio.pause();
                            audio.currentTime = 0;
                        }
                        if (correcto) {
                            jQuery(".container-wrapper").load("feedback-antes-correcta.html",function(){
                                var audio = document.getElementById("d32_correcto.mp3");
                                    audio.play();   
                            });
                        } else {
                            jQuery(".container-wrapper").load("feedback-antes-incorrecta.html",function(){
                                 var audio = document.getElementById("d32_incorrecto.mp3");
                                 audio.play();   
                            });
                        }
                    }
                }
            });
        };
    jQuery(".container-wrapper").delegate(".content-btn .btn","click",function(e){
          var audio = document.getElementById("d32.mp3");
          var val = $(this).data("value");
            $(this).siblings().removeClass("active-btn-green");
            $(this).addClass("active-btn-green");
            
            validate(audio);

            e.preventDefault();
    }); 
      
    };

    durante8();
});



