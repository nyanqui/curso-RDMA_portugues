$(document).ready(function(){
    var rptas = [false, false, false, false, false, false ]

    var posCO = [];

     //$(function () {
        var pastvideo1 = "";
        var pastvideo2 = "";
        var pastvideo3 = "";
        var pastvideo4 = "";
        var pastvideo5 = "";
        var pastvideo6 = "";
        var pastvideo7 = "";

        $("#co, #cpo, #desc, #det, #dett, #dettt").css({"top":"0px", "left": "0px"});


        $("#co").draggable({
            revert: "invalid",
            start: function () {
                $(".good, .bad").hide(500);
                Positioning.initialize($(this));
            }
        }).dblclick(function(){
            $(this).animate($(this).data().originalLocation, "slow");

            pastvideo1 = "";

            $(".good, .bad").hide(500);


        });

        $("#cpo").draggable({
           revert: "invalid",
           start: function () {
                $(".good, .bad").hide(500);
                Positioning.initialize($(this));
            }
        }).dblclick(function(){
            $(this).animate($(this).data().originalLocation, "slow");

            pastvideo2 = "";

            $(".good, .bad").hide(500);


        });

        $("#desc").draggable({
           revert: "invalid",
           start: function () {
                $(".good, .bad").hide(500);
                Positioning.initialize($(this));
            }
        }).dblclick(function(){
            $(this).animate($(this).data().originalLocation, "slow");

            pastvideo3 = "";

            $(".good, .bad").hide(500);


        });

        $("#det").draggable({
           revert: "invalid",
           start: function () {
                $(".good, .bad").hide(500);
                Positioning.initialize($(this));
            }
        }).dblclick(function(){
            $(this).animate($(this).data().originalLocation, "slow");

            pastvideo4 = "";

            $(".good, .bad").hide(500);


        });

        $("#dett").draggable({
           revert: "invalid",
           start: function () {
                $(".good, .bad").hide(500);
                Positioning.initialize($(this));
            }
        }).dblclick(function(){
            $(this).animate($(this).data().originalLocation, "slow");

            pastvideo5 = "";

            $(".good, .bad").hide(500);


        });

        $("#dettt").draggable({
            revert: "invalid",
            start: function () {
                $(".good, .bad").hide(500);
                Positioning.initialize($(this));
            }
        }).dblclick(function(){
            $(this).animate($(this).data().originalLocation, "slow");

            pastvideo6 = "";

            $(".good, .bad").hide(500);


        });
     

        $("#wrap1").droppable({
            //Event to accept a video when dropped on the droppable
            drop: function (event, ui) {

                $(this).addClass("ui-state-highlight").find("p").html(" ");

                //Get the current video object
                 currentvideo1 = $(ui.draggable).attr('id');

                pastvideo2 = (pastvideo2 == currentvideo1) ? "" : pastvideo2;
                pastvideo3 = (pastvideo3 == currentvideo1) ? "" : pastvideo3;
                pastvideo4 = (pastvideo4 == currentvideo1) ? "" : pastvideo4;
                pastvideo5 = (pastvideo5 == currentvideo1) ? "" : pastvideo5;
                pastvideo6 = (pastvideo6 == currentvideo1) ? "" : pastvideo6;
                pastvideo7 = (pastvideo7 == currentvideo1) ? "" : pastvideo7;

                rptas[0] = (currentvideo1 == "cpo") ? true : false;

                
                //If there is an object prior to the current one
                if (pastvideo1 != "" && pastvideo1 != currentvideo1) {
                    //Place past object into its original coordinate
                    $("#" + pastvideo1).animate($("#" + pastvideo1).data().originalLocation, "slow");

                }

                if (pastvideo1 == currentvideo1) {
                    validDrag(false);
                } else {
                    validDrag(true);
                }


                //Store the current video object
                pastvideo1 = currentvideo1;

                var $this = $(this);
                 ui.draggable.position({
                      my: "center ",
                      at: "center",
                      of: $this,
                      using: function(pos) {
                        $(this).animate(pos, 400, "linear");
                      }
                });
            }
        });

        $("#wrap2").droppable({
            //Event to accept a video when dropped on the droppable
            drop: function (event, ui) {

                $(this).addClass("ui-state-highlight").find("p").html(" ");

                //Get the current video object
                 currentvideo2 = $(ui.draggable).attr('id');

                pastvideo1 = (pastvideo1 == currentvideo2) ? "" : pastvideo1;
                pastvideo3 = (pastvideo3 == currentvideo2) ? "" : pastvideo3;
                pastvideo4 = (pastvideo4 == currentvideo2) ? "" : pastvideo4;
                pastvideo5 = (pastvideo5 == currentvideo2) ? "" : pastvideo5;
                pastvideo6 = (pastvideo6 == currentvideo2) ? "" : pastvideo6;
                pastvideo7 = (pastvideo7 == currentvideo2) ? "" : pastvideo7;

                 
                rptas[1] = (currentvideo2 == "desc") ? true : false;

                 

                //If there is an object prior to the current one
                if (pastvideo2 != "" && pastvideo2 != currentvideo2) {
                    //Place past object into its original coordinate
                    $("#" + pastvideo2).animate($("#" + pastvideo2).data().originalLocation, "slow");

                }

                if (pastvideo2 == currentvideo2) {
                    validDrag(false);
                } else {
                    validDrag(true);
                }

                //Store the current video object
                pastvideo2 = currentvideo2;

                 var $this = $(this);
                 ui.draggable.position({
                      my: "center",
                      at: "center",
                      of: $this,
                      using: function(pos) {
                        $(this).animate(pos, 400, "linear");
                      }
                });
            }
        });

        $("#wrap3").droppable({
            //Event to accept a video when dropped on the droppable
            drop: function (event, ui) {

                $(this).addClass("ui-state-highlight").find("p").html(" ");

                //Get the current video object
                 currentvideo3 = $(ui.draggable).attr('id');

                pastvideo1 = (pastvideo1 == currentvideo3) ? "" : pastvideo1;
                pastvideo2 = (pastvideo2 == currentvideo3) ? "" : pastvideo2;
                pastvideo4 = (pastvideo4 == currentvideo3) ? "" : pastvideo4;
                pastvideo5 = (pastvideo5 == currentvideo3) ? "" : pastvideo5;
                pastvideo6 = (pastvideo6 == currentvideo3) ? "" : pastvideo6;
                pastvideo7 = (pastvideo7 == currentvideo3) ? "" : pastvideo7;

                 
                 rptas[2] = (currentvideo3 == "co") ? true : false;

                 

                //If there is an object prior to the current one
                if (pastvideo3 != "" && pastvideo3 != currentvideo3) {
                    //Place past object into its original coordinate
                    $("#" + pastvideo3).animate($("#" + pastvideo3).data().originalLocation, "slow");

                }

                if (pastvideo3 == currentvideo3) {
                    validDrag(false);
                } else {
                    validDrag(true);
                }

                //Store the current video object
                pastvideo3 = currentvideo3;

                 var $this = $(this);
                 ui.draggable.position({
                      my: "center",
                      at: "center",
                      of: $this,
                      using: function(pos) {
                        $(this).animate(pos, 400, "linear");
                      }
                });
            }
        });

        $("#wrap4").droppable({
            //Event to accept a video when dropped on the droppable
            drop: function (event, ui) {

                $(this).addClass("ui-state-highlight").find("p").html(" ");

                //Get the current video object
                 currentvideo4 = $(ui.draggable).attr('id');

                pastvideo1 = (pastvideo1 == currentvideo4) ? "" : pastvideo1;
                pastvideo2 = (pastvideo2 == currentvideo4) ? "" : pastvideo2;
                pastvideo3 = (pastvideo3 == currentvideo4) ? "" : pastvideo3;
                pastvideo5 = (pastvideo5 == currentvideo4) ? "" : pastvideo5;
                pastvideo6 = (pastvideo6 == currentvideo4) ? "" : pastvideo6;
                pastvideo7 = (pastvideo7 == currentvideo4) ? "" : pastvideo7;
                 
                 rptas[3] = (currentvideo4 == "dettt") ? true : false;

                 

                //If there is an object prior to the current one
                if (pastvideo4 != "" && pastvideo4 != currentvideo4) {
                    //Place past object into its original coordinate
                    $("#" + pastvideo4).animate($("#" + pastvideo4).data().originalLocation, "slow");

                }

                if (pastvideo4 == currentvideo4) {
                    validDrag(false);
                } else {
                    validDrag(true);
                }

                //Store the current video object
                pastvideo4 = currentvideo4;

                var $this = $(this);
                ui.draggable.position({
                    my: "center",
                    at: "center",
                    of: $this,
                    using: function(pos) {
                        $(this).animate(pos, 400, "linear");
                    }
                });
            }
        });

        $("#wrap5").droppable({
            //Event to accept a video when dropped on the droppable
            drop: function (event, ui) {

                $(this).addClass("ui-state-highlight").find("p").html(" ");

                //Get the current video object
                 currentvideo5 = $(ui.draggable).attr('id');

                pastvideo1 = (pastvideo1 == currentvideo5) ? "" : pastvideo1;
                pastvideo2 = (pastvideo2 == currentvideo5) ? "" : pastvideo2;
                pastvideo3 = (pastvideo3 == currentvideo5) ? "" : pastvideo3;
                pastvideo5 = (pastvideo5 == currentvideo5) ? "" : pastvideo5;
                pastvideo6 = (pastvideo6 == currentvideo5) ? "" : pastvideo6;
                pastvideo7 = (pastvideo7 == currentvideo5) ? "" : pastvideo7;
                 
                 rptas[4] = (currentvideo5 == "det") ? true : false;

                 

                //If there is an object prior to the current one
                if (pastvideo5 != "" && pastvideo5 != currentvideo5) {
                    //Place past object into its original coordinate
                    $("#" + pastvideo5).animate($("#" + pastvideo5).data().originalLocation, "slow");

                }

                if (pastvideo5 == currentvideo5) {
                    validDrag(false);
                } else {
                    validDrag(true);
                }

                //Store the current video object
                pastvideo5 = currentvideo5;

                var $this = $(this);
                ui.draggable.position({
                    my: "center",
                    at: "center",
                    of: $this,
                    using: function(pos) {
                        $(this).animate(pos, 400, "linear");
                    }
                });
            }
        });




        $("#wrap6").droppable({
            //Event to accept a video when dropped on the droppable
            drop: function (event, ui) {

                $(this).addClass("ui-state-highlight").find("p").html(" ");

                //Get the current video object
                 currentvideo6 = $(ui.draggable).attr('id');

                pastvideo1 = (pastvideo1 == currentvideo6) ? "" : pastvideo1;
                pastvideo2 = (pastvideo2 == currentvideo6) ? "" : pastvideo2;
                pastvideo3 = (pastvideo3 == currentvideo6) ? "" : pastvideo3;
                pastvideo4 = (pastvideo4 == currentvideo6) ? "" : pastvideo4;
                pastvideo5 = (pastvideo5 == currentvideo6) ? "" : pastvideo5;
                pastvideo7 = (pastvideo7 == currentvideo6) ? "" : pastvideo7;
                 
                 rptas[5] = (currentvideo6 == "dett") ? true : false;

                 

                //If there is an object prior to the current one
                if (pastvideo6 != "" && pastvideo6 != currentvideo6) {
                    //Place past object into its original coordinate
                    $("#" + pastvideo6).animate($("#" + pastvideo6).data().originalLocation, "slow");

                }

                if (pastvideo6 == currentvideo6) {
                    validDrag(false);
                } else {
                    validDrag(true);
                }

                //Store the current video object
                pastvideo6 = currentvideo6;

                var $this = $(this);
                ui.draggable.position({
                    my: "center",
                    at: "center",
                    of: $this,
                    using: function(pos) {
                        $(this).animate(pos, 400, "linear");
                    }
                });
            }
        });
        
    //});

    
    var Positioning = (function () {
        return {
            //Initializes the starting coordinates of the object
            initialize: function (object) {
                object.data("originalLocation", $(this).originalPosition = { top: 0, left: 0 });
            },
            //Resets the object to its starting coordinates
            reset: function (object) {
                object.data("originalLocation").originalPosition = { top: 0, left: 0 };
            },
        };
    })();


    var _timeOut;

    var validDrag = function (flag) {
        if (flag) {
            clearTimeout(_timeOut);
            _timeOut = setTimeout(function(){
                var coPositions = JSON.stringify($("#co").css(["top", "left"]));
                var cpoPositions = JSON.stringify($("#cpo").css(["top", "left"]));
                var descPositions = JSON.stringify($("#desc").css(["top", "left"]));
                var detPositions = JSON.stringify($("#det").css(["top", "left"]));
                var dettPositions = JSON.stringify($("#dett").css(["top", "left"]));
                var detttPositions = JSON.stringify($("#dettt").css(["top", "left"]));
                var audioCorrecto = document.getElementById('d15_correcto.mp3');
                var audioIncorrecto = document.getElementById('d15_incorrecto.mp3');
                var nextPage = document.getElementById('nextPage');
                console.log("nextPage",nextPage)
                var truePos = JSON.stringify({"top":"0px","left":"0px"});

                if (coPositions != truePos && cpoPositions != truePos && descPositions != truePos && detPositions != truePos && dettPositions != truePos && detttPositions != truePos) {

                    if (rptas[0] && rptas[1] && rptas[2] && rptas[3] && rptas[4] && rptas[5]) {
                        $(".bad").hide();
                        $(".good").show(500);
                        audioIncorrecto.pause();
                        audioIncorrecto.currentTime = 0;
                        audioCorrecto.play();
                        if(nextPage){
                            nextPage.removeAttribute('disabled');
                            nextPage.classList.remove('notClicked');
                            nextPage.setAttribute('href','feedback-antes.html');
                        }
                        

                    } else {

                        $(".good").hide();
                        $(".bad").show(500);
                        audioCorrecto.pause();
                        audioCorrecto.currentTime = 0;
                        audioIncorrecto.play();
                        if(nextPage){
                            nextPage.setAttribute('disabled','disabled');
                            nextPage.classList.add('notClicked');
                            nextPage.setAttribute('href','#');
                        }

                    }
                }
            }, 700);
        }
    };

    $(".bad").on("click", function (e) {
        rptas = [false, false, false, false, false, false];

        pastvideo1 = "";
        pastvideo2 = "";
        pastvideo3 = "";
        pastvideo4 = "";
        pastvideo5 = "";
        pastvideo6 = "";
        pastvideo7 = "";

        $(".good, .bad").hide(500);

        $("#co, #cpo, #desc, #det, #dett, #dettt").animate($("#co").data().originalLocation, "slow");

        e.preventDefault();
    });

    if (!($(".lt-ie9").length)) {
            TweenLite.to(".tl_title", 1, {opacity: 1, delay:.5});

            TweenLite.from(".tl_block1", 1, {scaleX:.4, scaleY:.4, delay:1.5, ease: "Back.easeOut"});
            TweenLite.to(".tl_block1", 1, {opacity: 1, delay:1.5});

            TweenLite.from(".tl_block2", 1, {x:-300, delay:2.5, ease: "Back.easeOut"});
            TweenLite.to(".tl_block2", 1, {opacity: 1, delay:2.5});

            TweenLite.from(".tl_block3", 1, {x:-300, delay:3.5, ease: "Back.easeOut"});
            TweenLite.to(".tl_block3", 1, {opacity: 1, delay:3.5});
        }/**/


}); 