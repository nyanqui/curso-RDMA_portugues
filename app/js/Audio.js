var Audio = {
	data : null,
	autoRun : false,
	defaultDelay: 0,
	
	init : function(data){
	
		if(data && data.length > 0){
			var self = this;			
			self.setData(data);
		}else{
			console.log("No se puede iniciarlizar Audio");
		}
	},

	setData : function(data){
		var self = this;
		self.data = data;
		self.run();
		//self.createAudioElements();
	},
	stopAll : function (){
        
        var elmentsAudio = jQuery(".audio-item","#reproductor");        
        for (var i = 0; i < elmentsAudio.length; i++) {
             if(elmentsAudio[i].currentTime > 0 ){
	             elmentsAudio[i].pause();
	             elmentsAudio[i].currentTime = 0;
             }
        }
        return true;
    },
	/**
	 * [createAudioElements crea los elementos Audio en el DOM]
	 * @return {[type]} [description]
	 */
	createAudioElements : function(){
	  
	   var self = this;
	   var localData = self.data;
	   
	   for (var i = 0; i < localData.length; i++) {
	   		var audioData = localData[i];
	   		var element = document.createElement("audio"); 
	   		     element.id       = audioData.id;		      
		         element.src      = audioData.audio;
		         element.type     = audioData.audioType;
		         

			document.getElementById('media-container').appendChild(element);      
		     
	   }

	   self.run();
	},
	
	run : function(){
	   
	   var self = this;
	   var localData = self.data;
	   
	   for (var i = 0; i < localData.length; i++) {
	   	   (function () {
	   	   	  	
	   	   	  	var audioData = localData[i];    
		        var element = document.getElementById(audioData.id);
		     
		        var firstDelay = (audioData.delay) ? audioData.delay : self.defaultDelay;
		        var itemsChild = (audioData.whitChild && audioData.whitChild.length > 0)? audioData.whitChild : [];
		        
		        if( audioData.isFirst ){
		        	
		        	if(audioData.class){
			        	
			        	var optionsTo = (audioData.options && audioData.options.to != undefined)? audioData.options.to : null;
			        	var optionsFrom = (audioData.options && audioData.options.from != undefined)? audioData.options.from : null;  
			        	var time = (audioData.time && audioData.time != undefined )?audioData.time:1;
			        	if(optionsTo != null){
			        		TweenLite.to(audioData.class, time, optionsTo);
			            }
			            if(optionsFrom != null){
			            	TweenLite.from(audioData.class, time,optionsFrom);		            			            	
			            }
			    	}
		        	//self.stopAll();
		        	setTimeout(function(){		
		            	element.play();		            	
		            },firstDelay);

		        }

		        if( itemsChild.length > 0 ){ 		            
		            for (var x = 0; x < itemsChild.length; x++) {
		            	var child = itemsChild[x];
		            	var childTime = (child.time && child.time != undefined)?child.time:1;
		            	if(child.options.to){
		            		TweenLite.to(child.class, childTime, child.options.to);
		            	}
		            	if(child.options.from){
		            		TweenLite.from(child.class, childTime,child.options.from);			            	
		            	}
		        	}
			    }  
		        if(  audioData.onEnd != undefined ){
		                  
		           var nexTAudio = audioData.onEnd.audio;
		           var nextClass = audioData.onEnd.class;
		           var delay  	 = (audioData.onEnd.delay != undefined)? audioData.onEnd.delay : self.defaultDelay;
		           var ele 		 = document.getElementById(nexTAudio);

		           var nextOptionsTo = (audioData.onEnd.options && audioData.onEnd.options.to)? audioData.onEnd.options.to : {opacity: 1, delay: 0 };
		           var nextOptionsFrom = (audioData.onEnd.options && audioData.onEnd.options.from)? audioData.onEnd.options.from : {scaleX:.4, scaleY:1, ease: "Back.easeOut"};
		        
		           var childOnEnd = (audioData.onEnd.whitChild && audioData.onEnd.whitChild.length > 0 )? audioData.onEnd.whitChild : [];

		           	element.onended = function(){   
		            
		          
		             if(ele){
		              //self.stopAll();
				         setTimeout(function(){
				         	ele.play();
				         },delay);
				       
				        if(nextClass){
			                TweenLite.to(nextClass,1,nextOptionsTo);
			                TweenLite.from(nextClass,1,nextOptionsFrom);
		           	 	}
		           	 }
		           	
		           	if( childOnEnd.length > 0 ){
		           	 	 for (var s = 0; s < childOnEnd.length;s++) {
			            	var child = childOnEnd[s];
			            	TweenLite.to(child.class, 1, child.options.to);
			            	TweenLite.from(child.class, 1,child.options.from);			            	
			            }
		           	 }
		           	 
		           	
		           }
		       }

	   	   }()); 
	   }
	}
};